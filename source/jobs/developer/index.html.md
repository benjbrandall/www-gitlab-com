---
layout: job_page
title: "Developer"
---

At GitLab, developers are highly independent and self-organized individual
contributors who work together as a tight team in a [remote and agile](/2015/09/14/remote-agile-at-gitlab/) way.

Most backend developers work on all aspects of GitLab, building features, fixing bugs, and generally improving the application.
Some developers [specialize](/jobs/specialist) and focus on a specific area, such as packaging, performance or GitLab CI.
Developers can specialize immediately after joining, or after some time, when they have gained familiarity with many areas of GitLab and find one they would like to focus on.

## Responsibilities

* Develop features from proposal to polished end result.
* Support our [service engineers](/jobs/service-engineer) in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the core team and the open source community and collaborate on improving GitLab.
* Review code contributed by the rest of the community and work with them to get it ready for production.
* Write documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to GitLab.
* Qualify developers for hiring.

## Requirements

* You can reason about software, algorithms and performance from a high level
* You are passionate about open source
* You have worked on a production-level Rails application
* You know how to write your own Ruby gem using TDD techniques
* You share our [values](/handbook/#values), and work in accordance with those values.

We typically hire people who have experience in programming languages used
at GitLab (e.g. Ruby on Rails, Go), but we welcome candidates who have
excellent experience in other languages and frameworks. Tackling a GitLab CE
issue is a good way to demonstrate your ability to learn and debug.

## Technical interview

As part of our technical interview<a name="technical-interview"></a>, you will
be asked to pick an issue from the GitLab CE issue tracker, and code 'live' with
the interviewer there to talk with and collaborate with. We do this because we
believe that it is the best way for you to see what the work is really like, and
for our [interviewer to see how you think, code, and collaborate](http://zachholman.com/posts/startup-interviewing-is-fucked/#collaborate).
When contributing code, you should follow the [Contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md),
and you agree to the [individual contributor license agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md).
If you prefer not to do the above please let us know and we'll give you an assignment that does not relate to GitLab but does test the relevant skills.
 
## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/#gitlab-workflow).

## Senior Developers

Senior Developers are experienced developers who:

* know a domain really well and radiate that knowledge
* train new people
* are the go-to person when people on the team have development questions

## Internships

We normally don't offer any internships, but if you get a couple of merge requests
accepted we'll interview you for one. This will be a remote internship without
supervision, you'll only get feedback on your merge requests. If you want to
work on open source and qualify please [submit an application](https://gitlab.workable.com/jobs/207439/candidates/new).
In the cover letter field please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the hiring manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.