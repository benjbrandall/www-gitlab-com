---
layout: job_page
title: "Production Engineer"
---

Production engineers work on the infrastructure team that runs our services, including GitLab.com.

## Responsibilities

* As an Production Engineer you will be on a pager duty rotation to respond
to GitLab.com availability incidents and customer incidents.
* Improve GitLab.com availability and performance
* Incident response for customers and for GitLab.com
* Making GitLab easier to maintain for sysadmins all over the world
* Manage our infrastructure with Chef (and a little Ansible)
* Improve monitoring systems
* Improve deployment processes

## Workflow

* You work on issues in the [infrastructure repository](https://gitlab.com/gitlab-com/infrastructure/issues).
* The priority of the issues can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).

## Requirements for Applicants

* Linux experience (we use Ubuntu Server)
* Database experience (we use PostgreSQL and Redis)
* Chef experience
* Ruby scripting experience (our preferred language for operations scripts)
* Programming experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/#values), and work in accordance with those values.
